<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// страницы для пользователей
Route::get('/', 'PagesController@index');
Route::get('about', 'PagesController@about');
Route::get('reviews', 'PagesController@reviews');
Route::get('contact', 'PagesController@contact');
Route::get('news', 'PagesController@news');
Route::get('news/{id}', 'PagesController@show');
Route::get('events', 'PagesController@events');
Route::get('events/{id}', 'PagesController@eventShow');


// ресурс новостей в админ панеде
Route::resource('admin/article', 'ArticlesController')->middleware('auth');
// редактирование слайдера на главной
Route::resource('admin/slider', 'SlidersController')->middleware('auth');
// контакты фирмы
Route::resource('admin/contact', 'ContactsController')->middleware('auth');
Route::resource('admin/diplom', 'DiplomsController')->middleware('auth');
Route::resource('admin/about', 'AboutController')->middleware('auth');
Route::resource('admin/event', 'EventsController')->middleware('auth');


Route::get('admin', 'AdminController@index')->middleware('auth');
// данные с формы о клиентах
Route::get('admin/clients', 'AdminController@clients')->middleware('auth');


// получение данных клиента с формы ajax
Route::post('clients', 'PagesController@addClient');

Route::get('sitemap.xml', 'PagesController@sitemap');