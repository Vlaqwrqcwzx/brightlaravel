<?php
namespace App;
use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;
use Laratrust;
use App\Article;

class MyMenuFilter implements FilterInterface
{
    public function transform($item, Builder $builder)
    {
        if (isset($item['permission']) && ! Laratrust::can($item['permission'])) {
            return false;
        }

        if (isset($item['url']) && $item['url'] == 'admin/article') {
        	$item['label'] = Article::count();
        }

        return $item;
    }
}