<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
    	'address',
    	'email',
    	'phone1',
    	'phone2',
    	'fax',
    	'site',
    	'vk',
    	'instagram',
        'telegram'
    ];
}
