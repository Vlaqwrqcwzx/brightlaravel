<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diplom extends Model
{
    protected $fillable = [
    	'title',
        'original_filename',
        'filename',
        'mime'
    ];
}
