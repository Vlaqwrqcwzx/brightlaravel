<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
    	'title',
    	'body',
    	'href_title',
    	'href',
    	'button_active',
    	'filename',
    	'mime',
        'original_filename'
    ];

}
