<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class SlidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::all();
        return view('sliders.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // сохранение изображения
        if (!empty($request->file('bookcover')))
        {
            $cover = $request->file('bookcover');
            $extension = $cover->getClientOriginalExtension();
            Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));

            $request['original_filename'] = $cover->getClientOriginalName();
            $request['filename'] = $cover->getFilename().'.'.$extension;
            $request['mime'] = $cover->getClientMimeType();
        }

        flash('Слайд был успешно добавлен')->success()->important();
        Slider::create($request->all());
        return redirect('admin/slider'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('sliders.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        // сохранение изображения
        if (!empty($request->file('bookcover')))
        {
            $cover = $request->file('bookcover');
            $extension = $cover->getClientOriginalExtension();
            Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));

            $request['original_filename'] = $cover->getClientOriginalName();
            $request['filename'] = $cover->getFilename().'.'.$extension;
            $request['mime'] = $cover->getClientMimeType();
        }

        flash('Слайд успешно был обновлен')->success()->important();
        $slider->update($request->all());
        return redirect('admin/slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $slider->delete();
        flash('Слайд успешно был удален')->success()->important();
        return redirect('admin/slider');
    }
}
