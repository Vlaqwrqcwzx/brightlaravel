<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Client;

use App\Contact;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.admin');
    }

    public function clients()
    {
        $clients = Client::latest('created_at')->paginate(15);
        return view('admin.clients',compact('clients'));
    }

}
