<?php

namespace App\Http\Controllers;

use App\Event;
use App\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Requests\CreateEventRequest;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::latest('published_at')->latest('created_at')->paginate(4);
        foreach ($events as $event) {
            //берем первую картинку для превью события
            $event['image'] = Event::find($event['id'])->images()->first();
        }
        return view('events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEventRequest $request, Image $file)
    {
        
        $id = Event::create($request->all())->id;

        // сохранение изображений
        if (!empty($request->file('bookcover')))
        {
            $covers = $request->file('bookcover');
            foreach ($covers as $cover) {
                $extension = $cover->getClientOriginalExtension();
                Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));
                $file->create([
                    'event_id' => $id,
                    'original_filename' => $cover->getClientOriginalName(),
                    'filename' => $cover->getFilename().'.'.$extension,
                    'mime' => $cover->getClientMimeType()
                ]);
            }
        }

        flash('Событие успешно было создано')->success()->important();
        return redirect('admin/event');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $result, $id)
    {
        $event = $result::findOrFail($id);
        $images = Event::find($id)->images()->get();
        return view('events.show', compact(['event','images']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $images = Event::find($event->id)->images()->get();
        return view('events.edit', compact(['event','images']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEventRequest $request, Event $event, Image $file)
    {
        $id = $event->id;

        // сохранение изображений
        if (!empty($request->file('bookcover')))
        {
            $covers = $request->file('bookcover');
            foreach ($covers as $cover) {
                $extension = $cover->getClientOriginalExtension();
                Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));
                $file->create([
                    'event_id' => $id,
                    'original_filename' => $cover->getClientOriginalName(),
                    'filename' => $cover->getFilename().'.'.$extension,
                    'mime' => $cover->getClientMimeType()
                ]);
            }
        }

        $event->update($request->all());
        flash('Событие успешно было обновленно')->success()->important();
        return redirect('admin/event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event->delete();
        flash('Событие успешно было удалено')->success()->important();
        return redirect('admin/event');
    }
}
