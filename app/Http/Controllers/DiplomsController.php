<?php

namespace App\Http\Controllers;

use App\Diplom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class DiplomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diploms = Diplom::latest('created_at')->paginate(4);
        return view('diploms.index', compact('diploms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('diploms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // сохранение изображения
        if (!empty($request->file('bookcover')))
        {
            $cover = $request->file('bookcover');
            $extension = $cover->getClientOriginalExtension();
            Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));

            $request['original_filename'] = $cover->getClientOriginalName();
            $request['filename'] = $cover->getFilename().'.'.$extension;
            $request['mime'] = $cover->getClientMimeType();
        }

        flash('Награда успешно была добавлена')->success()->important();
        Diplom::create($request->all());
        return redirect('/admin/diplom');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Diplom  $diplom
     * @return \Illuminate\Http\Response
     */
    public function show(Diplom $diplom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Diplom  $diplom
     * @return \Illuminate\Http\Response
     */
    public function edit(Diplom $diplom)
    {
        return view('diploms.edit', compact('diplom'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Diplom  $diplom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Diplom $diplom)
    {
        // сохранение изображения
        if (!empty($request->file('bookcover')))
        {
            $cover = $request->file('bookcover');
            $extension = $cover->getClientOriginalExtension();
            Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));

            $request['original_filename'] = $cover->getClientOriginalName();
            $request['filename'] = $cover->getFilename().'.'.$extension;
            $request['mime'] = $cover->getClientMimeType();
        }

        flash('Награда успешно была обновлена')->success()->important();
        $diplom->update($request->all());
        return redirect('admin/diplom');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Diplom  $diplom
     * @return \Illuminate\Http\Response
     */
    public function destroy(Diplom $diplom)
    {
        $diplom->delete();
        flash('Награда успешно была удалена')->success()->important();
        return redirect('/admin/diplom');
    }
}
