<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests\CreateArticleRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::latest('published_at')->latest('created_at')->paginate(4);
        return view('articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateArticleRequest $request, Article $article)
    {
        // сохранение изображения
        if (!empty($request->file('bookcover')))
        {
            $cover = $request->file('bookcover');
            $extension = $cover->getClientOriginalExtension();
            Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));

            $request['original_filename'] = $cover->getClientOriginalName();
            $request['filename'] = $cover->getFilename().'.'.$extension;
            $request['mime'] = $cover->getClientMimeType();
        }

        flash('Новость успешно была добавлена')->success()->important();
        Article::create($request->all());
        return redirect('admin/article'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $result, $id)
    {
        $article = $result::findOrFail($id);
        return view('articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view('articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(CreateArticleRequest $request, Article $article)
    {
        // сохранение изображения
        if (!empty($request->file('bookcover')))
        {
            $cover = $request->file('bookcover');
            $extension = $cover->getClientOriginalExtension();
            Storage::disk('public')->put($cover->getFilename().'.'.$extension,  File::get($cover));

            $request['original_filename'] = $cover->getClientOriginalName();
            $request['filename'] = $cover->getFilename().'.'.$extension;
            $request['mime'] = $cover->getClientMimeType();
        }

        flash('Новость успешно была обновлена')->success()->important();
        $article->update($request->all());
        return redirect('admin/article');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        flash('Новость успешно была удалена')->success()->important();
        return redirect('admin/article');
    }
}
