<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Article;

use App\Client;

use App\Slider;

use App\Diplom;

use App\About;

use App\Event;

use App\Image;

class PagesController extends Controller
{
    public function index()
    {
        $articles = Article::orderBy('published_at', 'desc')->latest('created_at')->take(3)->get();
        $sliders = Slider::all();
        $diploms = Diplom::latest('created_at')->get();
    	return view('pages.index', compact('articles', 'sliders', 'diploms'));
    }

    public function about()
    {
        $about = About::first();
    	return view('pages.about', compact('about'));
    }

    public function news()
    {
        $articles = Article::latest('published_at')->latest('created_at')->published()->paginate(6);
        return view('pages.news', compact('articles'));
    }

    public function reviews()
    {
        return view('pages.reviews');
    }

    public function contact()
    {
    	return view('pages.contact');
    }

    public function show($id)
    {
        $article = Article::findOrFail($id);
        return view('pages.news-detail', compact('article'));
    }

    public function addClient(Request $request)
    {
        Client::create($request->all());
        return "Ok";
    }

    public function events()
    {
        $events = Event::latest('published_at')->latest('created_at')->paginate(4);
        foreach ($events as $event) {
            //берем первую картинку для превью события
            $event['image'] = Event::find($event['id'])->images()->first();
        }
        return view('pages.events', compact('events'));
    }

    public function eventShow(Event $result, $id)
    {
        $event = $result::findOrFail($id);
        $images = Event::find($id)->images()->get();
        return view('pages.event-detail', compact(['event','images']));
    }

    public function sitemap()
    {
        $articles = Article::all();
        $events = Event::all();
        return view('sitemap')->with(compact(['articles','events']));
    }

}
