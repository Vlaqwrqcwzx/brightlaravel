<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	protected $table = 'files';

    protected $fillable = [
        'event_id', 
        'filename', 
        'mime',
        'original_filename',
    ];
}
