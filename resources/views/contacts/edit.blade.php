@extends('adminlte::page')

@section('title', 'Редактирование контактов')

@section('content_header')
<h1>Редактирование контактов</h1>
@stop

@section('content')
<section class="content">
	<div class="row">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Редактирование контактов</h3>
			</div>

			<form role="form" method="post" action="{{ action('ContactsController@update', $contact) }}" accept-charset="UTF-8" enctype="multipart/form-data">
				@method('patch')
				<div class="box-body">


					<div class="form-group">
					<label>Адресс</label>
					<input name="address" type="text" class="form-control" placeholder="Адресс" value="{{ $contact->address }}" required>
					</div>

					<div class="form-group">
					<label>Email</label>
					<input name="email" type="text" class="form-control" placeholder="Email" value="{{ $contact->email }}" required>
					</div>

					<div class="form-group">
					<label>Телефон №1</label>
					<input name="phone1" type="text" class="form-control" placeholder="Телефон №1" value="{{ $contact->phone1 }}" required>
					</div>

					<div class="form-group">
					<label>Телефон №2</label>
					<input name="phone2" type="text" class="form-control" placeholder="Телефон №2" value="{{ $contact->phone2 }}" required>
					</div>

					<div class="form-group">
					<label>Факс</label>
					<input name="fax" type="text" class="form-control" placeholder="Факс" value="{{ $contact->fax }}" required>
					</div>

					<div class="form-group">
					<label>Сайт</label>
					<input name="site" type="text" class="form-control" placeholder="Сайт" value="{{ $contact->site }}" required>
					</div>

					<div class="form-group">
					<label>vk</label>
					<input name="vk" type="text" class="form-control" placeholder="vk" value="{{ $contact->vk }}" required>
					</div>

					<div class="form-group">
					<label>instagram</label>
					<input name="instagram" type="text" class="form-control" placeholder="instagram" value="{{ $contact->instagram }}" required>
					</div>

					<div class="form-group">
					<label>telegram</label>
					<input name="telegram" type="text" class="form-control" placeholder="telegram" value="{{ $contact->telegram }}" required>
					</div>

				</div>
				<div class="box-footer">
					<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
					<input type="submit" value="Обновить" class="btn btn-primary">
				</div>
			</form>

		</div>
	</div>
</section>

@include('errors.errors')

<div class="box-footer text-center">
	<a href="{{ url('/admin/contact') }}" class="pad_a">Назад</a>
</div>

@stop