@extends('adminlte::page')

@section('title', 'Контакты')

@section('content_header')
<h1>Контакты</h1>
@stop

@section('content')
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Список контактов</h3>
				</div>
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover contact-table">
						<tbody>
							<tr>
								<th><i class="fa fa-map-marker" aria-hidden="true"></i> <span class="color-span">Адресс</span></th>
								<td>{{ $contact->address }}</td>
							</tr>
							<tr>
								<th><i class="fa fa-reply" aria-hidden="true"></i> <span class="color-span">Email</span></th>
								<td>{{ $contact->email }}</td>
							</tr>
							<tr>
								<th><i class="fa fa-mobile" aria-hidden="true"></i> <span class="color-span">Телефон №1</span></th>
								<td>{{ $contact->phone1 }}</td>
							</tr>
							<tr>
								<th><i class="fa fa-mobile" aria-hidden="true"></i> <span class="color-span">Телефон №2</span></th>
								<td>{{ $contact->phone2 }}</td>
							</tr>
							<tr>
								<th><i class="fa fa-fax" aria-hidden="true"></i> <span class="color-span">Факс</span></th>
								<td>{{ $contact->fax }}</td>
							</tr>
							<tr>
								<th><i class="fa fa-sitemap" aria-hidden="true"></i> <span class="color-span">Сайт</span></th>
								<td>{{ $contact->site }}</td>
							</tr>
							<tr>
								<th><i class="fa fa-vk" aria-hidden="true"></i> <span class="color-span">vk</span></th>
								<td>{{ $contact->vk }}</td>
							</tr>
							<tr>
								<th><i class="fa fa-instagram" aria-hidden="true"></i> <span class="color-span">instagram</span></th>
								<td>{{ $contact->instagram }}</td>
							</tr>
							<tr>
								<th><i class="fa fa-telegram" aria-hidden="true"></i> <span class="color-span">telegram</span></th>
								<td>{{ $contact->telegram }}</td>
							</tr>	
						</tbody>
					</table>
				</div>
				<div class="box-footer">
					<a href="{{ action('ContactsController@edit', $contact) }}" class="btn btn-primary">Редактировать</a>
				</div>
			</div>
		</div>
	</div>
</section>
@stop