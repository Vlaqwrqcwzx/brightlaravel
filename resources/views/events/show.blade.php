@extends('adminlte::page')

@section('title', $event->title)

@section('content')

<h1>{!!$event->title!!}</h1>
<hr>
<div>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		@php ($tmp = 1)
		@foreach($images as $image)
		<li data-target="#carousel-example-generic" data-slide-to="1" class="@if($tmp == 1)  {{$tmp = 0}} active @endif"></li>
	    @endforeach
	</ol>

	<div class="carousel-inner">
		@php ($tmp = 1)
		@foreach($images as $image)
		<div class="item @if($tmp == 1) active @endif">
		@php ($tmp = 0)
		<img src="{{url('uploads/'.$image->filename)}}" alt="slide">
		</div>
		@endforeach
	</div>

	<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
	  <span class="fa fa-angle-left"></span>
	</a>
	<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
	  <span class="fa fa-angle-right"></span>
	</a>
</div>
	
</div>
<div>
	{!!$event->body!!}
</div>

@stop