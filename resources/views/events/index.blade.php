@extends('adminlte::page')

@section('title', 'События')

@section('content_header')
<h1>События</h1>
@stop

@section('content')
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@include('flash::message')
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Список событий</h3>
				</div>
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th>Заголовок</th>
								<th>Статья</th>
								<th>Дата публикации</th>
								<th>Превью</th>
								<th></th>
							</tr>
							
							@foreach ($events as $event)
							<tr>
								<td>
									<a href="{{ action('EventsController@show', $event->id) }}">{{$event->title}}</a>
								</td>
								<td>{{$event->body}}</td>
								<td>{{$event->published_at->day}} {{$event->published_at->formatLocalized('%B')}}  {{$event->published_at->year}}</td>
								<td><img class="news-detail-image" src="{{url('uploads/'.$event->image['filename'])}}" alt="{{$event->image['original_filename']}}"></td>
								<td>
									<a href="{{ action('EventsController@edit', $event) }}" class="btn btn-block btn-primary">Редактировать</a>
								</td>
								<td>
									<form method="post" action="{{ action('EventsController@destroy', $event) }}" accept-charset="UTF-8" class="btn-block">
										<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
										 <input type="hidden" name="_method" value="delete" />
									    <input class="btn btn-danger btn-block" type="submit" value="Удалить" /> 
									</form>
								</td>
							</tr>
							@endforeach

						</tbody>
					</table>
				</div>
				<div class="text-center">
	            	{{ $events->links() }}
	        	</div>
			</div>
		</div>
	</div>
</section>
@stop