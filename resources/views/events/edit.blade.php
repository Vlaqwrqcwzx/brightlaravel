@extends('adminlte::page')

@section('title', 'Редактирование события'.$event->title)

@section('content_header')
<h1>Редактирование события {{ $event->title }}</h1>
@stop

@section('content')
<section class="content">

	<div class="row">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Редактирование события</h3>
			</div>


			<form role="form" method="post" action="{{ action('EventsController@update', $event) }}" accept-charset="UTF-8" enctype="multipart/form-data">
				@method('patch')
				<div class="box-body">
					<div class="form-group">
					<label>Заголовок</label>
					<input name="title" type="text" class="form-control" placeholder="Заголовок" value="{{ $event->title }}" required>
					</div>

					<div class="form-group">
					<label>Статья</label>
					<textarea name="body" rows="10" class="form-control" required>{{ $event->body }}</textarea>
					</div>

					<div class="form-group">
					<label>Дата публикации</label>
					<input name="published_at" type="date" class="form-control"  value="{{$event->published_at->format('Y-m-d')}}">
					</div>

					<div class="form-group">
						<label>Изображения:</label>
					</div>

					<div class="form-group">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							@php ($tmp = 1)
							@foreach($images as $image)
							<li data-target="#carousel-example-generic" data-slide-to="1" class="@if($tmp == 1)  {{$tmp = 0}} active @endif"></li>
						    @endforeach
						</ol>

						<div class="carousel-inner">
							@php ($tmp = 1)
							@foreach($images as $image)
							<div class="item @if($tmp == 1)  {{$tmp = 0}} active @endif">
							<img src="{{url('uploads/'.$image->filename)}}" alt="slide">
							</div>
							@endforeach
						</div>

						<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
						  <span class="fa fa-angle-left"></span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
						  <span class="fa fa-angle-right"></span>
						</a>
					</div>
					</div>

					<div class="form-group">
						<label>Добавить изображения: (множественный выбор)</label>
					</div>
				    <div class="form-group">
				        <input type="file" class="form-control" name="bookcover[]" multiple/>
				    </div>

				</div>
				<div class="box-footer">
					<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
					<input type="submit" value="Обновить" class="btn btn-primary">
				</div>
			</form>




		</div>
	</div>
</section>

@include('errors.errors')

<div class="box-footer text-center">
	<a href="{{ url('/admin/event') }}" class="pad_a">Назад</a>
</div>

@stop