@extends('adminlte::page')

@section('title', 'Добавление события')

@section('content_header')
<h1>Добавление события</h1>
@stop

@section('content')
<section class="content">
	<div class="row">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Добавление события</h3>
			</div>

			<form role="form" method="post" action="{{ action('EventsController@store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
				<div class="box-body">
					<div class="form-group">
					<label>Заголовок</label>
					<input name="title" type="text" class="form-control" placeholder="Заголовок" value="{{ @old('title') }}" required>
					</div>

					<div class="form-group">
					<label>Статья</label>
					<textarea name="body" rows="10" class="form-control" value="{{ @old('body') }}" required></textarea>
					</div>

					<div class="form-group">
					<label>Дата публикации</label>
					<input name="published_at" type="date" class="form-control"  value="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
					</div>

				    <div class="form-group">
				        <label>Изображения: (множественный выбор)</label>
				        <input type="file" class="form-control" name="bookcover[]" multiple/>
				    </div>
				</div>
				<div class="box-footer">
					<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
					<input type="submit" value="Добавить" class="btn btn-success">
				</div>
			</form>

		</div>
	</div>
</section>

@include('errors.errors')

<div class="box-footer text-center">
	<a href="{{ url('/admin/event') }}" class="pad_a">Назад</a>
</div>

@stop