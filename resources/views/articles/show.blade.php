@extends('adminlte::page')

@section('title', $article->title)

@section('content')

<h1>{{$article->title}}</h1>
<hr>
<div>
	<img src="{{url('uploads/'.$article->filename)}}" alt="">
</div>
<div>
	{{$article->body}}
</div>

@stop