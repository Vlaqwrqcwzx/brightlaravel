@extends('adminlte::page')

@section('title', 'Редактирование информации о компании')

@section('content_header')
<h1>Редактирование информации о компании</h1>
@stop

@section('content')
<section class="content">
	<div class="row">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Редактирование информации о компании</h3>
			</div>

			<form role="form" method="post" action="{{ action('AboutController@update', $about) }}" accept-charset="UTF-8" enctype="multipart/form-data">
				@method('patch')
				<div class="box-body">
					<div class="form-group">
					<label>Заголовок</label>
					<input name="title" type="text" class="form-control" placeholder="Заголовок" value="{{ $about->title }}" required>
					</div>
					<div class="form-group">
					<label>Статья</label>
					<textarea name="body" rows="10" class="form-control" required>{{ $about->body }}</textarea>
					</div>
				</div>
				<div class="box-footer">
					<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
					<input type="submit" value="Обновить" class="btn btn-primary">
				</div>
			</form>

		</div>
	</div>
</section>

@include('errors.errors')

<div class="box-footer text-center">
	<a href="{{ url('/admin/about') }}" class="pad_a">Назад</a>
</div>

@stop