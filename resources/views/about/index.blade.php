@extends('adminlte::page')

@section('title', 'О компании')

@section('content_header')
<h1>О компании</h1>
@stop

@section('content')
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@include('flash::message')
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">О компании</h3>
				</div>
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th>Заголовок</th>
								<td>{{$about->title}}</td>
							</tr>
							<tr>
								<th>Описание</th>
								<td>{{$about->body}}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="box-footer">
					<a href="{{ action('AboutController@edit', $about) }}" class="btn btn-primary">Редактировать</a>
				</div>
			</div>
		</div>
	</div>
</section>
@stop