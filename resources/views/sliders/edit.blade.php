@extends('adminlte::page')

@section('title', 'Редактирование слайда')

@section('content_header')
<h1>Редактирование слайда</h1>
@stop

@section('content')
<section class="content">
	<div class="row">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Редактирование слайда</h3>
			</div>

			<form role="form" method="post" action="{{ action('SlidersController@update', $slider) }}" accept-charset="UTF-8" enctype="multipart/form-data">
				@method('patch')
				<div class="box-body">
					<div class="form-group">
					<label>Заголовок</label>
					<input name="title" type="text" class="form-control" placeholder="<span>Оранжевый</span> белый" value="{{ $slider->title }}" required>
					</div>

					<div class="form-group">
					<label>Описание</label>
					<textarea name="body" rows="5" class="form-control" required>{{ $slider->body }}</textarea>
					</div>
					
					<div class="checkbox">
						<label>
							<input type="checkbox" name="button_active" @if($slider->button_active == 'on') checked @endif>
							Добавлять кнопку на слайдер
						</label>
					</div>	

					<div class="but-slide-active">
						<hr>
						<div class="form-group">
						<label>Заголовок кнопки (записаться на консультацию)</label>
						<input name="href_title" type="text" class="form-control" placeholder="{{ $slider->href_title }}" value="{{ $slider->href_title }}" required>
						</div>					

						<div class="form-group">
						<label>Ссылка перехода с кнопки</label>
						<input name="href" type="text" class="form-control" placeholder="Ссылка перехода с кнопки" value="{{ $slider->href }}" required>
						</div>
						<hr>
					</div>

					<div class="form-group">
						<label>Изображение:</label>
					</div>
					<div class="form-group">
						<img class="news-detail-image" src="{{url('uploads/'.$slider->filename)}}" alt="">
					</div>
				    <div class="form-group">
				        <input type="file" class="form-control" name="bookcover" value="" />
				    </div>

				</div>

				<div class="box-footer">
					<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
					<input type="submit" value="Обновить" class="btn btn-primary">
				</div>
			</form>

		</div>
	</div>
</section>
@stop

@section('js')
<script>
	$( "input[name='button_active']" ).change(function(){
		if($(this).is(":checked")) {
			$('.but-slide-active').fadeIn('slow');
		}
		else {
			$('.but-slide-active').fadeOut('slow');
		}
	});
</script>
@stop