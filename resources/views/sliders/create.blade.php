@extends('adminlte::page')

@section('title', 'Добавление слайда')

@section('content_header')
<h1>Добавление слайда</h1>
@stop

@section('content')
<section class="content">
	<div class="row">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Добавление слайда</h3>
			</div>

			<form role="form" method="post" action="{{ action('SlidersController@store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
				<div class="box-body">
					<div class="form-group">
					<label>Заголовок</label>
					<input name="title" type="text" class="form-control" placeholder="Заголовок" value="{{ @old('title') }}" required>
					</div>

					<div class="form-group">
					<label>Описание</label>
					<textarea name="body" rows="5" class="form-control" value="{{ @old('body') }}" required></textarea>
					</div>
					
					<div class="checkbox">
						<label>
							<input type="checkbox" name="button_active" checked>
							Добавлять кнопку на слайдер
						</label>
					</div>	

					<div class="but-slide-active">
						<hr>
						<div class="form-group">
						<label>Заголовок кнопки (записаться на консультацию)</label>
						<input name="href_title" type="text" class="form-control" placeholder="Записаться на консультацию" value="Записаться на консультацию" required>
						</div>					

						<div class="form-group">
						<label>Ссылка перехода с кнопки</label>
						<input name="href" type="text" class="form-control" placeholder="Ссылка перехода с кнопки" value="#" required>
						</div>
						<hr>
					</div>

				    <div class="form-group">
				        <label>Изображение:</label>
				        <input type="file" class="form-control" name="bookcover"/>
				    </div>
				</div>
				<div class="box-footer">
					<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
					<input type="submit" value="Добавить" class="btn btn-success">
				</div>
			</form>

		</div>
	</div>
</section>
@stop

@section('js')
<script>
	$( "input[name='button_active']" ).change(function(){
		if($(this).is(":checked")) {
			$('.but-slide-active').fadeIn('slow');
		}
		else {
			$('.but-slide-active').fadeOut('slow');
		}
	});
</script>
@stop