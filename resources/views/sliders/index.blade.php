@extends('adminlte::page')

@section('title', 'Слайдер на главной')

@section('content_header')
<h1>Слайдер на главной</h1>
@stop

@section('content')
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@include('flash::message')
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Список слайдов</h3>
				</div>
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th>Заголовок</th>
								<th>Описание</th>
								<th>Заголовок кнопки</th>
								<th>Ссылка перехода с кнопки</th>
								<th>Дата создания</th>
								<th>Изображение</th>
								<th></th>
							</tr>
							
							@foreach ($sliders as $slide)
							<tr>
								<td>{{$slide->title}}</td>
								<td>{{$slide->body}}</td>
								<td>{{$slide->href_title}}</td>
								<td>{{$slide->href}}</td>
								<td>{{$slide->created_at->day}} {{$slide->created_at->formatLocalized('%B')}}  {{$slide->created_at->year}}</td>
								<td><img class="news-detail-image" src="{{url('uploads/'.$slide->filename)}}" alt="{{$slide->filename}}"></td>
								<td>
									<a href="{{ action('SlidersController@edit', $slide) }}" class="btn btn-block btn-primary">Редактировать</a>
								</td>
								<td>
									<form method="post" action="{{ action('SlidersController@destroy', $slide) }}" accept-charset="UTF-8" class="btn-block">
										<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
										 <input type="hidden" name="_method" value="delete" />
									    <input class="btn btn-danger btn-block" type="submit" value="Удалить" /> 
									</form>
								</td>
							</tr>
							@endforeach

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@stop