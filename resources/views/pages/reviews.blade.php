@extends('app')

@section('title', '- Отзывы')

@section('seo')
<meta name="description" content="BRIGHT CAPITAL Отзывы">
@stop

@section('content')
<!-- breadcumb-area start -->
<div class="breadcumb-area flex-style  black-opacity">
    <div class="container" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <div class="row">
            <div class="col-12">
                <h2>Отзывы</h2>
                <ul class="d-flex">
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="Главная" href="/">
                                <span itemprop="name">Главная</span>
                                <meta itemprop="position" content="1">
                            </a>
                        </span>
                    </li>
                    <li><i class="fa fa-angle-double-right"></i></li>
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="Отзывы" href="/reviews">
                                <span itemprop="name" class="white">Отзывы</span>
                                <meta itemprop="position" content="2">
                            </a>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- breadcumb-area end -->

<!-- testmonial-area start -->
<div class="testmonial-area" id="reviews">
    <div class="col-12">
        <div class="section-title text-center mbdif">
            <h2>Отзывы Клиентов</h2>
            <img src="assets/images/line.png" alt="">
        </div>
    </div>
    <div class="test-active owl-carousel">
        <div class="test-items">
            <div class="container">
                <div class="row" style="justify-content: center;">
					<video src="{{url('uploads/vid/%D0%92%D0%B8%D0%B4%D0%B5%D0%BE%201.mp4')}}" width="80%" controls></video>
                </div>
            </div>
        </div>
        <div class="test-items">
            <div class="container">
                <div class="row" style="justify-content: center;">
					<video src="{{url('uploads/vid/Видео%202.mp4')}}" width="80%" controls></video>
                </div>
            </div>
        </div>
        <div class="test-items">
            <div class="container">
                <div class="row" style="justify-content: center;">
                    <video src="{{url('uploads/vid/Видео%203.mp4')}}" width="80%" controls></video>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="testmonial-area">
<div class="container pb">
    <div id="wpac-review"></div>
    <script type="text/javascript">
    wpac_init = window.wpac_init || [];
    wpac_init.push({widget: 'Review', id: 16549});
    (function() {
        if ('WIDGETPACK_LOADED' in window) return;
        WIDGETPACK_LOADED = true;
        var mc = document.createElement('script');
        mc.type = 'text/javascript';
        mc.async = true;
        mc.src = 'https://embed.widgetpack.com/widget.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
    })();
    </script>
</div>
</div>
<!-- testmonial-area end -->
@stop