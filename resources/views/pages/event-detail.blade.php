@extends('app')

@section('seo')
<meta name="description" content="<?php echo mb_strimwidth($event->body, 0, 160, "...");?>">
@stop


@section('title', '- '.$event->title)

@section('content')
<!-- breadcumb-area start -->
<div class="breadcumb-area flex-style  black-opacity">
    <div class="container" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <div class="row">
            <div class="col-12">
                <h2>{{ $event->title }}</h2>
                <ul class="d-flex">
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="Главная" href="/">
                                <span itemprop="name">Главная</span>
                                <meta itemprop="position" content="1">
                            </a>
                        </span>
                    </li>
                    <li><i class="fa fa-angle-double-right"></i></li>
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="События" href="/events">
                                <span itemprop="name">События</span>
                                <meta itemprop="position" content="2">
                            </a>
                        </span>
                    </li>
                    <li><i class="fa fa-angle-double-right"></i></li>
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="{{ $event->title }}" href="{{ url('events/'.$event->id) }}">
                                <span itemprop="name" class="white">{{ $event->title }}</span>
                                <meta itemprop="position" content="3">
                            </a>    
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- breadcumb-area end -->

<!-- blog-details-area start -->
<div class="blog-details-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-details-wrap">


<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">

	@php ($tmp = 1)
	@foreach($images as $image)
    <div class="carousel-item @if($tmp == 1) active @endif">
    	@php ($tmp = 0)
      <img class="d-block w-100 mh-image" src="{{url('uploads/'.$image->filename)}}" alt="slide">
    </div>
    @endforeach

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

                    <h3>{{$event->title}}</h3>
                    <ul>
                        <li><span class="hoverd-date">{{$event->published_at->day}} {{$event->published_at->formatLocalized('%B')}}, {{$event->published_at->year}}</span></li>
                    </ul>
                    <p>{!!$event->body!!}</p>
                </div>
                <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                <script src="//yastatic.net/share2/share.js"></script>
                <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter,viber,whatsapp,skype,telegram" data-counter=""></div>
            </div>
        </div>
    </div>
</div>
<!-- blog-details-area end -->

@stop
