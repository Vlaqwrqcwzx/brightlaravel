@extends('app')

@section('title', '- Контакты')

@section('seo')
<meta name="description" content="BRIGHT CAPITAL Контакты">
@stop

@section('content')
<!-- breadcumb-area start -->
<div class="breadcumb-area flex-style  black-opacity">
    <div class="container" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <div class="row">
            <div class="col-12">
                <h2>Контакты</h2>
                <ul class="d-flex">
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="Главная" href="/">
                                <span itemprop="name">Главная</span>
                                <meta itemprop="position" content="1">
                            </a>
                        </span>
                    </li>
                    <li><i class="fa fa-angle-double-right"></i></li>
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="О Компании" href="/contact">
                                <span itemprop="name" class="white">Контакты</span>
                                <meta itemprop="position" content="2">
                            </a>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- breadcumb-area end -->


    <div class="contact-page-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact-page-info">
                        <h3>Офис</h3>
                        <ul class="border-bottom">
                            <li><i class="fa fa-home"></i>{{ $contacts['contacts']['address'] }}</li>
                            <li><i class="fa fa-envelope"></i>{{ $contacts['contacts']['email'] }}</li>
                            <li><i class="fa fa-phone"></i>{{ $contacts['contacts']['phone1'] }}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="contact-form">
                        <h3><span>Свяжись</span> с нами! </h3>
                        <p>Наши менеджеры с удовольствием проконсультируют Вас по Вашему проекту.</p>
                            <div class="row">
                                <div class="col-12">
                                    <button data-toggle="modal" data-target="#exampleModal">Заказать консультацию</button>
                                </div>
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="mbdif">
        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aee80740657ece7943eae09df614fc132ce959747d36ddc020632b76db8072b63&amp;source=constructor" width="100%" height="600" frameborder="0"></iframe>
    </div>
@stop

@section('js')
@stop