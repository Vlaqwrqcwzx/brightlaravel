@extends('app')

@section('seo')
<meta name="description" content="<?php echo mb_strimwidth($article->body, 0, 160, "...");?>">
@stop

@section('title', '- '.$article->title)

@section('content')

<!-- breadcumb-area start -->
<div class="breadcumb-area flex-style  black-opacity">
    <div class="container" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <div class="row">
            <div class="col-12">
                <h2>{{ $article->title }}</h2>
                <ul class="d-flex">
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="Главная" href="/">
                                <span itemprop="name">Главная</span>
                                <meta itemprop="position" content="1">
                            </a>
                        </span>
                    </li>
                    <li><i class="fa fa-angle-double-right"></i></li>
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="Новости" href="/news">
                                <span itemprop="name">Новости</span>
                                <meta itemprop="position" content="2">
                            </a>
                        </span>
                    </li>
                    <li><i class="fa fa-angle-double-right"></i></li>
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="{{ $article->title }}" href="{{ action('PagesController@show', $article->id) }}">
                                <span itemprop="name" class="white">{{ $article->title }}</span>
                                <meta itemprop="position" content="3">
                            </a>    
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- breadcumb-area end -->

<!-- blog-details-area start -->
<div class="blog-details-area">
    <div class="container" itemscope itemtype="http://schema.org/NewsArticle">
        <div class="row">
            <div class="col-12">
                <div class="blog-details-wrap">
                    <img itemprop="image" src="{{url('uploads/'.$article->filename)}}" alt="">
                    <h3 itemprop="headline">{{$article->title}}</h3>
                    <ul>
                    <li><span class="hoverd-date">{{$article->published_at->day}} {{$article->published_at->formatLocalized('%B')}}, {{$article->published_at->year}}</span></li>
                    <meta itemprop="datePublished" content="{{ $article->published_at }}">
                    <meta itemprop="dateModified" content="{{ $article->updated_at }}">
                    <meta itemprop="author" content="Bright Capital">
                    <meta itemprop="name" content="{{$article->title}}">
                    <p style="display: none;" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                        <span itemprop="name">Bright Capital</span>
                        <span itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
                        <img src="/assets/images/favicon.png"/>
                        <a itemprop="url" href="/assets/images/favicon.png"></a>
                        </span>
                    </p>                    
                    </ul>
                    <p itemprop="description">{!!$article->body!!}</p>
                </div>
                <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                <script src="//yastatic.net/share2/share.js"></script>
                <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter,viber,whatsapp,skype,telegram" data-counter=""></div>
            </div>
        </div>
    </div>
</div>
<!-- blog-details-area end -->

@stop
