@extends('app')

<?php 
    if(isset($_GET['page']))
        { $title_page = '- События Компании | Страница '.$_GET['page'];}
    else {
         $title_page = '- События Компании';
     } 
?>

@section('title')
<?php echo $title_page; ?>
@stop

@section('seo')
<meta name="description" content="BRIGHT CAPITAL <?php echo $title_page; ?>">
@stop

@section('content')

<!-- breadcumb-area start -->
<div class="breadcumb-area flex-style  black-opacity">
    <div class="container" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <div class="row">
            <div class="col-12">
                <h2>События Компании</h2>
                <ul class="d-flex">
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="Главная" href="/">
                                <span itemprop="name">Главная</span>
                                <meta itemprop="position" content="1">
                            </a>
                        </span>
                    </li>
                    <li><i class="fa fa-angle-double-right"></i></li>
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="События" href="/events">
                                <span itemprop="name" class="white">События</span>
                                <meta itemprop="position" content="2">
                            </a>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- breadcumb-area end -->

<!-- blog-area start -->
<div class="blog-area">
    <div class="container">
        <div class="row">
@foreach($events as $event)
	<div class="col-lg-4  col-sm-6 col-12" itemscope itemtype="http://schema.org/Event">
	    <div class="blog-wrap">
	        <div class="blog-img">
	            <img itemprop="image" src="{{url('uploads/'.$event->image['filename'])}}" alt="{{$event->image['filename']}}">
	        </div>
	        <div class="blog-content">
	            <h3><a href="{{ url('events/'.$event->id) }}" itemprop="name">{{$event->title}}</a></h3>
	            <ul>
	                <li><span class="hoverd-date">{{$event->published_at->day}} {{$event->published_at->formatLocalized('%B')}}, {{$event->published_at->year}}</span></li>
                    <meta itemprop="startDate" content="{{$event->published_at}}">
	            </ul>
	            <p itemprop="description"><?php echo mb_strimwidth($event->body, 0, 160, "...");?></p>
	            <a href="{{ url('events/'.$event->id) }}" class="readmore">Подробнее <i class="fa fa-angle-double-right"></i></a>
	        </div>
	    </div>
	</div>
@endforeach
        </div>
        <div class="row pagination-wrap">
            {{ $events->links() }}
        </div>
    </div>
</div>
<!-- blog-area end -->
@stop
