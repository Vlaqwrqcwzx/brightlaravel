@extends('app')

<?php 
    if(isset($_GET['page']))
        { $title_page = '- Новости | Страница '.$_GET['page'];}
    else {
         $title_page = '- Новости';
     } 
?>

@section('title')
<?php echo $title_page; ?>
@stop

@section('seo')
<meta name="description" content="BRIGHT CAPITAL <?php echo $title_page; ?>">
@stop

@section('content')

<!-- breadcumb-area start -->
<div class="breadcumb-area flex-style  black-opacity">
    <div class="container" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <div class="row">
            <div class="col-12">
                <h2>Новости Компании</h2>
                <ul class="d-flex">
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="Главная" href="/">
                                <span itemprop="name">Главная</span>
                                <meta itemprop="position" content="1">
                            </a>
                        </span>
                    </li>
                    <li><i class="fa fa-angle-double-right"></i></li>
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="Новости" href="/news">
                                <span itemprop="name" class="white">Новости</span>
                                <meta itemprop="position" content="2">
                            </a>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- breadcumb-area end -->

<!-- blog-area start -->
<div class="blog-area">
    <div class="container">
        <div class="row">
@foreach($articles as $article)
	<div class="col-lg-4  col-sm-6 col-12" itemscope itemtype="http://schema.org/NewsArticle">
	    <div class="blog-wrap">
	        <div class="blog-img">
	            <img itemprop="image" src="{{url('uploads/'.$article->filename)}}" alt="">
	        </div>
	        <div class="blog-content">
	            <h3 itemprop="headline"><a href="{{ action('PagesController@show', $article->id) }}">{{$article->title}}</a></h3>
	            <ul>
	                <li><span class="hoverd-date">{{$article->published_at->day}} {{$article->published_at->formatLocalized('%B')}}, {{$article->published_at->year}}</span></li>
                    <meta itemprop="datePublished" content="{{ $article->published_at }}">
                    <meta itemprop="dateModified" content="{{ $article->updated_at }}">
                    <meta itemprop="author" content="Bright Capital">
                    <meta itemprop="name" content="{{$article->title}}">
                    <p style="display: none;" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
                        <span itemprop="name">Bright Capital</span>
                        <span itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
                        <img src="/assets/images/favicon.png"/>
                        <a itemprop="url" href="/assets/images/favicon.png"></a>
                        </span>
                    </p>

	            </ul>
	            <p itemprop="description"><?php echo mb_strimwidth($article->body, 0, 160, "...");?></p>
	            <a href="{{ action('PagesController@show', $article->id) }}" class="readmore">Подробнее <i class="fa fa-angle-double-right"></i></a>
	        </div>
	    </div>
	</div>
@endforeach
        </div>
        <div class="row pagination-wrap">
            {{ $articles->links() }}
        </div>
    </div>
</div>
<!-- blog-area end -->
@stop
