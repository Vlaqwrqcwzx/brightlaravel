@extends('app')

@section('seo')
<meta name="description" content="BRIGHT CAPITAL КОНСАЛТИНГ В СФЕРАХ МИРОВОГО ФИНАНСОВОГО РЫНКА И ИНВЕСТИЦИЙ">
@stop

@section('head')
    <link rel="stylesheet" type="text/css" href="assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
@stop

@section('content')
<!-- slider-area start -->
<div class="slider-area">
    <div class="slider-active owl-carousel">

@php ($tpm = 0)
@foreach($sliders as $slide)
<div class="slider-items">
    <img src="{{url('uploads/'.$slide->filename)}}" alt="" class="slider">
    <div class="slider-content flex-style">
        <div class="container">
            <div class="row">
                <div class="@if($tpm == 0) {{$tpm = '1'}} col-xl-8 col-lg-10 col-12 @else {{$tpm = '0'}} offset-lg-4 col-lg-8 text-right col-12  @endif">
                    <h2>{!! $slide->title !!}</h2>
                    <p>{!! $slide->body !!}</p>
                    <ul>
                        <li class=""><a href="@if(isset($slide->href_title)){{ $slide->href }} @else # @endif" data-toggle="modal" data-target="#exampleModal">{!! $slide->href_title !!}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach

    </div>
</div>
<!-- slider-area end -->


<!-- service-area start -->
<div class="service-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2>Услуги Компании</h2>
                    <img src="assets/images/line.png" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="service-wrap ">
                    <h3>Что мы предлагаем</h3>
                    <p>Финансовый консалтинг – это профессиональная помощь инвесторам в решении инвестиционных задач, которую могут оказывать как частные специалисты, так и целые фирмы. Наша компания предлагает комплекс услуг в сфере финансового консультирования, позволяющих зарабатывать при любой ситуации на рынке. Независимо от вашего опыта торговли, вы можете обратиться к нашим экспертам. Они предложат наиболее выгодный вариант для инвестирования денежных средств с учетом ваших целей и возможностей. Узнайте больше о финансовом консалтинге и наших услугах на официальном сайте «Международного финансового центра».</p>
                    <a href="#" data-toggle="modal" data-target="#exampleModal">Записаться на консультацию</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <div class="service-items">
                    <span class="flaticon-bitcoin-9"></span>
                    <h3>Консультационное управление</h3>
                    <p>Консультационное управление — уникальная стратегия, при которой инвестор работает в тесной связке с личным финансовым консультантом.</p>
                </div>
                <div class="service-items">
                    <span class="flaticon-profits"></span>
                    <h3>Аналитика</h3>
                    <p>Разработка инвестиционных стратегий — услуга от экспертов нашего центра консалтинга и инвестиций, в ходе которой инвестор получает индивидуальную стратегию инвестирования.</p>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="row">
                    <div class="col-lg-12 col-md-6 col-12">
                        <div class="service-items">
                            <span class="flaticon-exchange-1"></span>
                            <h3>Портфельное инвестирование</h3>
                            <p>Портфельное инвестирование — стратегия, целью которой является формирование сложносоставного инвестиционного продукта из ликвидных финансовых инструментов.</p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-6 col-12">
                        <div class="service-items">
                            <span class="flaticon-bitcoin-7"></span>
                            <h3>Инвестиционный консультант</h3>
                            <p>Инвестиционный консультант — услуга, гарантирующая индивидуальный подход и оперативную помощь личного инвестиционного консультанта в решении ваших вопросов.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- service-area end -->

<!-- spacial-area start -->
<div class="spacial-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-12">
                <h2>Наши Преимущества</h2>
                <div class="row">
                    <div class="col-md-5 col-sm-6 col-12">
                        <ul class="mb-20">
                            <li><i class="fa fa-check"></i> Лицензии и сертификаты </li>
                            <li><i class="fa fa-check"></i> Работаем с 2006 года </li>
                        </ul>
                    </div>
                    <div class="col-md-5 col-sm-6 col-12">
                        <ul>
                            <li><i class="fa fa-check"></i> Награды и достижения </li>
                            <li><i class="fa fa-check"></i> Условия работы с клиентами </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- spacial-area end -->


    <!-- blog-area start -->
<div class="blog-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2>Новости Компании</h2>
                    <img src="assets/images/line.png" alt="">
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($articles as $article)
                <div class="col-lg-4  col-sm-6 col-12">
                    <div class="blog-wrap">
                        <div class="blog-img">
                            <img src="{{url('uploads/'.$article->filename)}}" alt="">
                        </div>
                        <div class="blog-content">
                            <h3><a href="{{ action('PagesController@show', $article->id) }}">{{ $article->title }}</a></h3>
                            <ul>
                                <li><span class="hoverd-date">{{$article->published_at->day}} {{$article->published_at->formatLocalized('%B')}}, {{$article->published_at->year}}</span></li>
                            </ul>
                            <p><?php echo mb_strimwidth($article->body, 0, 160, "...");?></p>
                            <a href="{{ action('PagesController@show', $article->id) }}" class="readmore">Подробнее <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="all-news-but">
                <a href="/news">Все новости</a>
            </div>
        </div>
    </div>
</div>
<!-- blog-area end -->


 <!-- about-area start -->
<div class="featured-content-area position-relative bg-1">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-6">
                <div class="featured-content-wrap">
                    <h2><span>Аналитика</span>Разработка инвестиционных стратегий</h2>
                    <p>Услуга для начинающих инвесторов и тех, кто хочет:<br>
                        Получить стратегию, отвечающую личным финансовым целям<br>
                        Получить опыт управления капиталом<br>
                        Заключать выгодные торговые сделки<br>
                        В результате услуги вы получите:<br>

                        Индивидуальную инвестиционную стратегию<br>
                        Детальные рекомендации по ее использованию<br>
                        Прогнозируемую доходность<br>
                        Круглосуточную поддержку</p>
                </div>
            </div>
        </div>
    </div>
    <div class="featured-content-img d-none d-lg-block">
        <img src="assets/images/about.png" alt="">
    </div>
</div>
<!-- about-area end -->


<!-- resorce-area start -->
<div class="resorce-area ptb-120">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2>Наши награды</h2>
                    <img src="assets/images/line.png" alt="">
                </div>
            </div>
        </div>
        <div class="diplom-container owl-carousel-dimplom owl-carousel">
        @foreach($diploms as $diplom)
            <a href="{{url('uploads/'.$diplom->filename)}}" class="fancybox">
                <img src="{{url('uploads/'.$diplom->filename)}}" alt="{{$diplom->filename}}">
            </a>
        @endforeach
        </div>
    </div>
</div>
@stop


@section('js')
<script type="text/javascript" src="assets/js/vendor/jquery.min.js"></script>
<script type="text/javascript" src="assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("a.fancybox").fancybox();
});
</script>
@stop

