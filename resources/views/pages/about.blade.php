@extends('app')

@section('title', '- О Компании')

@section('seo')
<meta name="description" content="BRIGHT CAPITAL О Компании">
@stop

@section('content')
<!-- breadcumb-area start -->
<div class="breadcumb-area flex-style  black-opacity">
    <div class="container" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <div class="row">
            <div class="col-12">
                <h2>О Компании</h2>
                <ul class="d-flex">
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="Главная" href="/">
                                <span itemprop="name">Главная</span>
                                <meta itemprop="position" content="1">
                            </a>
                        </span>
                    </li>
                    <li><i class="fa fa-angle-double-right"></i></li>
                    <li>
                        <span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="О Компании" href="/about">
                                <span itemprop="name" class="white">О Компании</span>
                                <meta itemprop="position" content="2">
                            </a>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- breadcumb-area end -->

    <!-- blog-details-area start -->
    <div class="blog-details-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="blog-details-wrap">
                        <h3>{{ $about->title }}</h3>
                        <p>{!! $about->body !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- blog-details-area end -->
@stop