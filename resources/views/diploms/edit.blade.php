@extends('adminlte::page')

@section('title', 'Редактирование награды')

@section('content_header')
<h1>Редактирование награды</h1>
@stop

@section('content')
<section class="content">
	<div class="row">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Редактирование награды</h3>
			</div>

			<form role="form" method="post" action="{{ action('DiplomsController@update', $diplom) }}" accept-charset="UTF-8" enctype="multipart/form-data">
				@method('patch')
				<div class="box-body">
					<div class="form-group">
					<label>Заголовок</label>
					<input name="title" type="text" class="form-control" placeholder="Заголовок" value="{{ $diplom->title }}" required>
					</div>

					<div class="form-group">
						<label>Изображение:</label>
					</div>
					<div class="form-group">
						<img class="news-detail-image" src="{{url('uploads/'.$diplom->filename)}}" alt="">
					</div>
				    <div class="form-group">
				        <input type="file" class="form-control" name="bookcover" value="" />
				    </div>

				</div>
				<div class="box-footer">
					<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
					<input type="submit" value="Обновить" class="btn btn-primary">
				</div>
			</form>

		</div>
	</div>
</section>

@include('errors.errors')

<div class="box-footer text-center">
	<a href="{{ url('/admin/diplom') }}" class="pad_a">Назад</a>
</div>

@stop