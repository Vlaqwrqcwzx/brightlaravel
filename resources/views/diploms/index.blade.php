@extends('adminlte::page')

@section('title', 'Награды')

@section('content_header')
<h1>Награды</h1>
@stop

@section('content')
<section class="content">
	<div class="row">
		<div class="col-xs-12">
		@include('flash::message')
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Список наград</h3>
				</div>
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
						@foreach($diploms as $diplom)
							<tr>
								<th>{{ $diplom->title }}</th>
								<td><img class="news-detail-image" src="{{url('uploads/'.$diplom->filename)}}" alt="{{$diplom->filename}}"></td>
								<td>
									<a href="{{ action('DiplomsController@edit', $diplom) }}" class="btn btn-block btn-primary">Редактировать</a>
								</td>
								<td>
									<form method="post" action="{{ action('DiplomsController@destroy', $diplom) }}" accept-charset="UTF-8" class="btn-block">
										<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
										 <input type="hidden" name="_method" value="delete" />
									    <input class="btn btn-danger btn-block" type="submit" value="Удалить" /> 
									</form>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
				<div class="text-center">
	            	{{ $diploms->links() }}
	        	</div>
			</div>
		</div>
	</div>
</section>
@stop