<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>BRIGHT CAPITAL @yield('title')</title>
    @yield('seo')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/images/favicon.png')}}">
    <link href='{{ url('sitemap.xml') }}' rel='alternate' title='Sitemap' type='application/rss+xml'/>
    <!-- Place favicon.ico in the root directory -->
    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css')}}">
    <!-- owl.carousel.2.0.0-beta.2.4 css -->
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css')}}">
    <!-- swiper.min.css -->
    <link rel="stylesheet" href="{{ asset('assets/css/swiper.min.css')}}">
    <!-- font-awesome v4.6.3 css -->
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css')}}">
    <!-- flaticon.css -->
    <link rel="stylesheet" href="{{ asset('assets/css/flaticon.css')}}">
    <!-- magnific-popup.css -->
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css')}}">
    <!-- metisMenu.min.css -->
    <link rel="stylesheet" href="{{ asset('assets/css/metisMenu.min.css')}}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css')}}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css')}}">
    <!-- modernizr css -->
    <script src="{{ asset('assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    @yield('head')
    <meta name="keywords" content="BRIGHT CAPITAL Саранск консалтинг в сферах мирового финансового рынка и инвестиций , экспертно аналитический центр">

        <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
       (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
       m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
       (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

       ym(52393459, "init", {
            id:52393459,
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true
       });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/52393459" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <meta name="yandex-verification" content="f3fb95905dc92d85" />
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    
    <!-- header-area start -->
    <header class="header-area">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-3 col-sm-4">
                        <div class="logo">
                            <a href="/"><img src="{{ asset('assets/images/logo.png')}}" alt=""><span class="logodes "><span class="logoBigText">BRIGHT CAPITAL</span> <span class="logoSmalText">экспертно аналитический центр</span></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-sm-8">
                        <ul class="contact-info d-flex justify-content-end">
                            <li>
                                <span><i class="fa fa-phone"></i>{{ $contacts['contacts']['phone1'] }}</span>
                                <span><i class="fa fa-fax"></i>{{ $contacts['contacts']['fax'] }}</span>
                            </li>
                            <li>
                                <span><i class="fa fa-envelope"></i>{{ $contacts['contacts']['email'] }}</span>
                                <span><i class="fa fa-paper-plane-o"></i>{{ $contacts['contacts']['site'] }}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-12 d-none d-lg-block">
                        <ul class="social-icon d-flex justify-content-end justify-content-sm-center">
                            <li class="facebook"><a href="{{ $contacts['contacts']['vk'] }}"><i class="fa fa-vk"></i></a></li>
                            <li class="google-plus"><a href="{{ $contacts['contacts']['instagram'] }}"><i class="fa fa-instagram"></i></a></li>
                            <li class="linkedin"><a href="tel:{{ $contacts['contacts']['telegram'] }}"><i class="fa fa-paper-plane-o"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-sm-9 d-none d-sm-block">
                        <ul class="mainmenu d-flex justify-content-end">
                            <li class="{{ request()->is('/') ? 'active' : '' }}"><a href="/">Главная</a>
                            </li>
                            <li class="{{ request()->is('about') ? 'active' : '' }}"><a href="/about">О компании</a></li>
                            <li class="{{ request()->is('news') ? 'active' : '' }}"><a href="/news">Новости</a>
                            </li>
                            <li class="{{ request()->is('events') ? 'active' : '' }}"><a href="/events">Cобытия</a></li>
                            <li class="{{ request()->is('reviews') ? 'active' : '' }}"><a href="/reviews">Отзывы</a>
                            </li>
                            <li class="{{ request()->is('contact') ? 'active' : '' }}"><a href="/contact">контакты</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-sm-3 col-8">
                        <div class="geta_quote">
                            <a href="#" data-toggle="modal" data-target="#exampleModal" class="">ЕСТЬ ВОПРОС ?</a>
                        </div>
                    </div>
                    <div class="d-block d-sm-none col-4 pull-right">
                        <ul class="menu">
                            <li class="first"></li>
                            <li class="second"></li>
                            <li class="third"></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- responsive-menu area start -->
            <div class="responsive-menu-area d-block d-sm-none">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <ul class="metismenu">
                                <li class="sidemenu-items"><a aria-expanded="false" href="/">ГЛАВНАЯ</a>
                                </li>
                                <li><a href="/about">О КОМПАНИИ</a></li>
                                <li><a href="/news">НОВОСТИ</a></li>
                                <li><a href="/events">СОБЫТИЯ</a></li>
                                <li><a href="/reviews">ОТЗЫВЫ</a></li>
                                <li><a href="/contact">КОНТАКТЫ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- responsive-menu area start -->
        </div>
        
    </header>
    <!-- header-area end -->

    @yield('content')

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Свяжись с нами! <p class="modal-desc">Наши менеджеры с удовольствием проконсультируют Вас по Вашему проекту.</p></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="contactForm" method="post">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <label for="name">Ваше имя:</label>
                            <input id="name" class="form-control" name="name" required type="text" placeholder="Иванов Иван Иванович">
                        </div>
                        <div class="form-group">
                            <label for="email">Ваш E-mail:</label>
                            <input id="email" class="form-control" name="email" required type="email" placeholder="ivanov@email.com">
                        </div>
                        <div class="form-group">
                            <label for="phone">Ваш телефон:</label>
                            <input id="phone" class="form-control" name="phone" required type="text" placeholder="+7 (800) 000-00-00">
                        </div>
                        <div class="form-group">
                            <label for="message">Текст сообщения:</label>
                            <textarea id="message" class="form-control" required name="message" rows="4"></textarea>
                        </div>
                        <div class="form-group form-check">
                            <input id="check" class="form-check-input" checked type="checkbox">
                            <label class="form-check-label" for="check">Я добровольно отправляю свои данные</label>
                        </div>
                        <button id="button" class="btn btn btn-warning orengBg" type="submit">Отправить</button>
                        <div class="result">
                            <span id="answer"></span>
                            <span id="loader" style="display:none"><img src="{{ asset('assets/images/loader.gif')}}" alt=""></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- footer-area start -->
    <footer class="footer-area">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3  col-sm-6 col-12">
                        <div class="footer-widget footer-logo">
                            <h1 class="fotName">BRIGHT CAPITAL</h1>
                            <p>КОНСАЛТИНГ В СФЕРАХ МИРОВОГО ФИНАНСОВОГО РЫНКА И ИНВЕСТИЦИЙ</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="footer-widget footer-menu">
                            <h3>Наши Услуги</h3>
                            <ul>
                                <li><a href="#" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-angle-double-right"></i> повышение финансовой грамотности клиентов</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-angle-double-right"></i> проведение консультаций в сфере инвестиций </a></li>
                                <li><a href="#" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-angle-double-right"></i> подготовка трейдеров на финансовых рынках</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-angle-double-right"></i> осуществление анализа банковского сектора и рынка криптовалют</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-angle-double-right"></i> подбор наилучших финансовых инструментов и создание оптимальных инвестиционных портфелей</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="footer-widget footer-twitter">
                            <h3>Новости Вконтакте</h3>
                            <ul>
                                <li><a href="https://vk.com/@brightcapital13-brexit-rezultaty-golosovaniya-i-dalneishie-perspektivy"><i class="fa fa-vk"></i>Brexit: результаты голосования и дальнейшие перспективы</a> </li>
                                <li class="mb-0"><a href="https://vk.com/@brightcapital13-brexit-rezultaty-golosovaniya-i-dalneishie-perspektivy"><i class="fa fa-vk"></i>ЦБ поднял ставку на 0,25% </a> </li>
                            </ul>
                        <div class="mt-30">
                        <a href="https://clck.yandex.ru/redir/dtype=stred/pid=7/cid=1228/*https://yandex.ru/pogoda/42" target="_blank"><img src="https://info.weather.yandex.net/42/2_white.ru.png?domain=ru" border="0" alt="Яндекс.Погода"/><img width="1" height="1" src="https://clck.yandex.ru/click/dtype=stred/pid=7/cid=1227/*https://img.yandex.ru/i/pix.gif" alt="" border="0"/></a>
                        </div>
                        <div class="mt-30">
                            <iframe  frameborder="no" scrolling="no" width="280" height="150" src="https://yandex.ru/time/widget/?geoid=42&lang=ru&layout=horiz&type=analog&face=serif"></iframe>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="footer-widget footer-contact">
                            <h3>Контакты</h3>
                            <ul class="footer-contact-info">
                                <li><span>Адрес: </span>{{ $contacts['contacts']['address'] }}</li>
                                <li><span>Email: </span>{{ $contacts['contacts']['email'] }}</li>
                                <li><span>Phone: </span>{{ $contacts['contacts']['phone1'] }}</li>
                            </ul>
                            <h5>Мы в социальных сетях</h5>
                            <ul class="social-icon d-flex">
                                <li class="facebook"><a href="{{ $contacts['contacts']['vk'] }}"><i class="fa fa-vk"></i></a></li>
                                <li class="google-plus"><a href="{{ $contacts['contacts']['instagram'] }}"><i class="fa fa-instagram"></i></a></li>
                                <li class="twitter"><a href="tel:{{ $contacts['contacts']['telegram'] }}"><i class="fa fa-paper-plane-o"></i></a></li>
                            </ul>
                        </div>
                        <!-- Yandex.Metrika informer -->
                        <a href="https://metrika.yandex.ru/stat/?id=52393459&amp;from=informer"
                        target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/52393459/3_0_FFFFFFFF_EFEFEFFF_0_pageviews"
                        style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="52393459" data-lang="ru" /></a>
                        <!-- /Yandex.Metrika informer -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer-area end -->
    <div style="display: none;" itemscope itemtype="http://schema.org/Organization">
        <span itemprop="name">BRIGHT CAPITAL</span>
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <span itemprop="streetAddress">Богдана Хмельницкого , 28</span>
            <span itemprop="postalCode"> 430005</span>
            <span itemprop="addressLocality">Саранск</span>
        </div>
        <span itemprop="telephone">{{ $contacts['contacts']['phone1'] }}</span>,
        <span itemprop="faxNumber">{{ $contacts['contacts']['fax'] }}</span>,
        <span itemprop="email">{{ $contacts['contacts']['email'] }}</span>
    </div>
    <!-- jquery latest version -->
    <script src="{{ asset('assets/js/vendor/jquery-2.2.4.min.js')}}"></script>
    <!-- popper.min.js -->
    <script src="{{ asset('assets/js/vendor/popper.min.js')}}"></script>
    <!-- bootstrap js -->
    <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- owl.carousel.2.0.0-beta.2.4 css -->
    <script src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>
    <!-- swiper.min.js -->
    <script src="{{ asset('assets/js/swiper.min.js')}}"></script>
    <!-- mailchimp.js -->
    <script src="{{ asset('assets/js/mailchimp.js')}}"></script>
    <!-- plugins js -->
    <script src="{{ asset('assets/js/jquery.canvasjs.min.js')}}"></script>
    <!-- metisMenu.min.js -->
    <script src="{{ asset('assets/js/metisMenu.min.js')}}"></script>
    <!-- plugins js -->
    <script src="{{ asset('assets/js/plugins.js')}}"></script>
    <!-- main js -->
    <script src="{{ asset('assets/js/scripts.js')}}"></script>
    <script src="{{ asset('assets/js/form.js')}}"></script>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'fLPPfkHzEs';var d=document;var w=window;function l(){
  var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
  s.src = '//code.jivosite.com/script/widget/'+widget_id
    ; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}
  if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}
  else{w.addEventListener('load',l,false);}}})();
</script>
<!-- {/literal} END JIVOSITE CODE -->

    @yield('js')
</body>
</html>