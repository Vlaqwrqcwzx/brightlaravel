{{ Request::header('Content-Type : text/xml') }}
<?php echo '<?xml version="1.0" encoding="UTF-8"?>';?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($articles as $article)
        <url>
            <loc>{{ action('PagesController@show', $article->id) }}</loc>
            <lastmod>{{ $article->updated_at->tz('GMT')->toAtomString() }}</lastmod>
            <changefreq>monthly</changefreq>
            <priority>1</priority>
        </url>
    @endforeach
    @foreach ($events as $event)
        <url>
            <loc>{{ url('events/'.$event->id) }}</loc>
            <lastmod>{{ $event->updated_at->tz('GMT')->toAtomString() }}</lastmod>
            <changefreq>monthly</changefreq>
            <priority>1</priority>
        </url>
    @endforeach
        <url>
            <loc>{{ url('/') }}</loc>
            <lastmod>2019-02-01</lastmod>
            <changefreq>monthly</changefreq>
            <priority>1</priority>
        </url>
        <url>
            <loc>{{ url('about') }}</loc>
            <lastmod>2019-02-01</lastmod>
            <changefreq>monthly</changefreq>
            <priority>1</priority>
        </url>
        <url>
            <loc>{{ url('reviews') }}</loc>
            <lastmod>2019-02-01</lastmod>
            <changefreq>monthly</changefreq>
            <priority>1</priority>
        </url>
        <url>
            <loc>{{ url('contact') }}</loc>
            <lastmod>2019-02-01</lastmod>
            <changefreq>monthly</changefreq>
            <priority>1</priority>
        </url>
        <url>
            <loc>{{ url('events') }}</loc>
            <lastmod>2019-02-01</lastmod>
            <changefreq>monthly</changefreq>
            <priority>1</priority>
        </url>
        <url>
            <loc>{{ url('news') }}</loc>
            <lastmod>2019-02-01</lastmod>
            <changefreq>monthly</changefreq>
            <priority>1</priority>
        </url>
</urlset>