@extends('adminlte::page')

@section('title', 'Обратная связь')

@section('content_header')
<h1>Обратная связь</h1>
@stop

@section('content')
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Список клиентов</h3>
				</div>
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th>ФИО</th>
								<th>Email</th>
								<th>Телефон</th>
								<th>Сообщение</th>
								<th>Дата обращения</th>
							</tr>
							
							@foreach ($clients as $client)
							<tr>
								<td>{{ $client->name }}</td>
								<td>{{ $client->email }}</td>
								<td>{{ $client->phone }}</td>
								<td>{{ $client->message }}</td>
								<td>{{$client->created_at->day}} {{$client->created_at->formatLocalized('%B')}}, {{$client->created_at->year}} , {{$client->created_at->format('H:i:s')}}</td>
							</tr>
							@endforeach

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="text-center">
    	{{ $clients->links() }}
	</div>
</section>
@stop